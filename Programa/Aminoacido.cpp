#include <iostream>
using namespace std;
#include "Aminoacido.h"

/* constructores */
Aminoacido::Aminoacido(){
  string nombre = "\0";
  int numero = 0;
  string atomos = "\0";
}

Aminoacido::Aminoacido(string nombre, int numero, string atomos){
  this->nombre = nombre;
  this->numero = numero;
  this->atomos = atomos;
}

/* get and set */
string Aminoacido::get_nombre(){
  return this->nombre;
}

int Aminoacido::get_numero(){
  return this->numero;
}

string Aminoacido::get_atomos(){
  return this->atomos;
}

void Aminoacido::set_nombre(string nombre){
  this->nombre = nombre;
}

void Aminoacido::set_numero(int numero){
  this->numero = numero;
}

void Aminoacido::set_atomos(string atomos){
  this->atomos = atomos;
}
