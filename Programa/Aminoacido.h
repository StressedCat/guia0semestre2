#include <iostream>
using namespace std;

#ifndef AMINOACIDO_H
#define AMINOACIDO_H

class Aminoacido {
    private:
      string nombre = "\0";
      int numero = 0;
      string atomos = "\0";

    public:
      /* constructores */
      Aminoacido ();
      Aminoacido (string nombre, int numero, string atomos);

      /* set and get */
      string get_nombre();
      int get_numero();
      string get_atomos();
      void set_nombre(string nombre);
      void set_numero(int numero);
      void set_atomos(string atomos);
};
#endif
