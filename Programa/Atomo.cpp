#include <iostream>
using namespace std;
#include "Atomo.h"

/* Constructores */
Atomo::Atomo() {
  string nombre = "\0";
  int numero = 0;
  string coordenadas = "\0";
}

Atomo::Atomo(string nombre, int numero, string coordenadas){
  this->nombre = nombre;
  this->numero = numero;
  this->coordenadas = coordenadas;
}

/* get y set*/
string Atomo::get_nombre(){
  return this->nombre;
}

int Atomo::get_numero(){
  return this->numero;
}

string Atomo::get_coordenadas(){
  return this->coordenadas;
}

void Atomo::set_nombre(string nombre){
  this->nombre = nombre;
}

void Atomo::set_numero(int numero){
  this->numero = numero;
}

void Atomo::set_coordenadas(string coordenadas){
  this->coordenadas = coordenadas;
}
