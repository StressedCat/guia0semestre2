#include <iostream>
using namespace std;

#ifndef ATOMO_H
#define ATOMO_H

class Atomo {
  private:
    string nombre = "\0";
    int numero = 0;
    string coordenadas = "\0";

  public:
    /* constructor */
    Atomo ();
    Atomo (string nombre, int numero, string coordenadas);

    /* set y get */
    string get_nombre();
    int get_numero();
    string get_coordenadas();
    void set_nombre(string nombre);
    void set_numero(int numero);
    void set_coordenadas(string coordenadas);
};
#endif
