#include <iostream>
using namespace std;
#include "Cadena.h"

/* constructores */
Cadena::Cadena(){
  string letra = "\0";
  string aminoacidos = "\0";
}

Cadena::Cadena(string letra, string aminoacidos){
  this->letra = letra;
  this->aminoacidos = aminoacidos;
}

/* get and set */
string Cadena::get_letra(){
  return this->letra;
}

string Cadena::get_aminoacidos(){
  return this->aminoacidos;
}

void Cadena::set_letra(string letra){
  this->letra = letra;
}

void Cadena::set_aminoacidos(string aminoacidos){
  this->aminoacidos = aminoacidos;
}
