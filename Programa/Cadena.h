#include <iostream>
using namespace std;

#ifndef CADENA_H
#define CADENA_H

class Cadena {
    private:
      string letra = "\0";
      string aminoacidos = "\0";

    public:
      /* Construir */
      Cadena ();
      Cadena (string letra, string aminoacido);

      /* set y get */
      string get_letra();
      string get_aminoacidos();
      void set_letra(string letra);
      void set_aminoacidos(string aminoacido);
};
#endif
