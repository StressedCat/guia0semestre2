/*
  * Para iniciar el programa:
  * g++ Programa.cpp Proteina.cpp Cadena.cpp Aminoacido.cpp Atomo.cpp Coordenada.cpp
  * Ejecutar:
  * ./a.out
  *
  * O
  * make
  *./Programa
  */
#include <iostream>
#include <list>
#include "Proteina.h"
#include "Cadena.h"
#include "Aminoacido.h"
#include "Atomo.h"
#include "Coordenada.h"
using namespace std;


/* Se imprime lo basico de la proteina */
void PrintProt(Proteina p){
  cout << "Informacion de la proteina: " << endl;
  cout << "Id: " << p.get_id() << endl;
  cout << "Nombre: " << p.get_nombre() << endl;
  cout << "Cadenas: " << endl;
}

/* se imprime una cadena de la proteina */
void PrintCad(Cadena c){
  cout << "Informacion de la cadena: " << endl;
  cout << "La cadena: " << c.get_letra() << endl;
  cout << "Letra del aminoacido: " << c.get_aminoacidos() << endl << endl;
}

/* se imprime un aminoacido que viene de la cadena */
void PrintAmin(Aminoacido a){
  cout << "Informacion del aminoacido: " << endl;
  cout << "Nombre del aminoacido: " << a.get_nombre() << endl;
  cout << "Posicion: " << a.get_numero() << endl;
  cout << "Atomos presentes: " << endl;
}

/* se imprime lo de los atomos de un aminoacido*/
void PrintAtom(Atomo a){
  cout << "Informacion del atomo: " << endl;
  cout << "Nombre del atomo: " << a.get_nombre() << endl;
  cout << "Posicion: " << a.get_numero() << endl;
  cout << "Coordenadas: " << endl;
}

/* se imprime la coordenada de un atomo */
void PrintCoord(Coordenada c, list<float> lista){
  int x = 0;
  for (float i: lista){
    if (x == 0) {
      c.set_x(i);
    }
    else if (x == 1) {
      c.set_y(i);
    }
    else if (x == 2) {
      c.set_z(i);
    }
    x = x + 1;
  }
  cout << "Coordenadas del atomo: " << endl;
  cout << "Eje x: " << c.get_x() << endl;
  cout << "Eje y: " << c.get_y() << endl;
  cout << "Eje z: " << c.get_z() << endl << endl;
}

void PrintLista(list<string> lista){
  for (string i: lista){
    cout << i << endl;
  }
  cout << endl;
}

int main(){
  /* Se agrega la informacion para cada sección
   *
   * Junto con agregar información, se usara la oportunidad de obtener
   * un resultado de la lista de vez en cuando, con la condicion que sea primero
   *
   * Se intentará ser lo mas explicito en el programa, mostrando que es cada
   * seccion
  */
  cout << endl;
  list<string> Proteinas;
  Proteinas.push_back("1CQ0");
  Proteinas.push_back("YUM3");
  Proteinas.push_back("P0G2");
  cout << "Se tienen las siguientes proteinas: " << endl;
  PrintLista(Proteinas);

  Proteina pro = Proteina("", "Hipocretina", "");
  string tmp = Proteinas.front();
  pro.set_id(tmp);
  list<string> Chain;
  Chain.push_back("PHE SER GLY PRO PRO GLY LEU GLN GLY ARG LEU GLN ARG");
  Chain.push_back("LEU LEU GLN ALA SER GLY ASN HIS ALA ALA GLY ILE LEU");
  Chain.push_back("THR MET");

  PrintProt(pro);
  PrintLista(Chain);

  tmp = Chain.front();
  Cadena cad = Cadena("", "PHE");
  cad.set_letra(tmp);

  PrintCad(cad);

  Aminoacido ami = Aminoacido("Fenilalanina", 1, "");
  list<string> elements;
  elements.push_back("C9");
  elements.push_back("H11");
  elements.push_back("N");
  elements.push_back("O2");
  PrintAmin(ami);
  PrintLista(elements);

  Atomo ato = Atomo("Carbono", 2, "");
  list<string> crdnds;
  crdnds.push_back("14.136");
  crdnds.push_back("-0.728");
  crdnds.push_back("9.641");
  PrintAtom(ato);
  PrintLista(crdnds);

  Coordenada coord = Coordenada(0, 0, 0);
  list<float> Ejes;
  Ejes.push_back(14.136);
  Ejes.push_back(-0.728);
  Ejes.push_back(9.641);
  PrintCoord(coord, Ejes);

  /* Se usa las secciones y se muestran explicitamente */
}
