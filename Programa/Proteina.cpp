#include <iostream>
using namespace std;
#include "Proteina.h"

/* constructores */
Proteina::Proteina(){
  string id = "\0";
  string nombre = "\0";
  string cadena = "\0";
}

Proteina::Proteina(string id, string nombre, string cadena){
  this->id = id;
  this->nombre = nombre;
  this->cadena = cadena;
}

/* get and set */
string Proteina::get_id(){
  return this->id;
}

string Proteina::get_nombre(){
  return this->nombre;
}

string Proteina::get_cadena(){
  return this->cadena;
}

void Proteina::set_id(string id){
  this->id = id;
}

void Proteina::set_nombre(string nombre){
  this->nombre = nombre;
}

void Proteina::add_cadena(string cadena){
  this->cadena = cadena;
}
