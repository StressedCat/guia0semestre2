#include <iostream>
using namespace std;

#ifndef PROTEINA_H
#define PROTEINA_H

class Proteina {
    private:
      string id = "\0";
      string nombre = "\0";
      string cadena = "\0";

    public:
      /* constructores */
      Proteina();
      Proteina(string id, string nombre, string cadena);

      /* set y get */
      string get_id();
      string get_nombre();
      string get_cadena();
      void set_id(string id);
      void set_nombre(string nombre);
      void add_cadena(string cadena);
};
#endif
